package br.com.itau.marketplace.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import br.com.itau.marketplace.models.Produto;

@Service
public class ProdutoDAO {
	Connection conexao;
	
	public ProdutoDAO() throws SQLException {
		conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/marketplace", "root", "root");
	}
	
	public List<Produto> buscarProdutos() throws SQLException {
		Statement statement = conexao.createStatement();
		ResultSet resultados = statement.executeQuery("SELECT * FROM produto");
		
		List<Produto> produtos = new ArrayList<>();
		
		while(resultados.next()) {
			produtos.add(criarProduto(resultados));
		}
		
		return produtos;
	}
	
	private Produto criarProduto(ResultSet resultados) throws SQLException {
		Produto produto = new Produto();
		produto.setId(resultados.getInt("id"));
		produto.setNome(resultados.getString("nome"));
		produto.setProprietario(resultados.getString("proprietario"));
		produto.setPreco(resultados.getDouble("preco"));
		
		return produto;
	}
	
	public void inserirProduto(Produto produto) throws SQLException{
		PreparedStatement statement = conexao.prepareStatement("INSERT INTO produto (nome, preco, proprietario) values (?, ?, ?)");
		statement.setString(1,  produto.getNome());
		statement.setDouble(2,  produto.getPreco());
		statement.setString(3,  produto.getProprietario());
		
		statement.executeUpdate();
	}
}
