package br.com.itau.marketplace.controllers;

import java.util.List;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.services.ProdutoDAO;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	@Autowired
	ProdutoDAO produtoService;

	@GetMapping
		public List<Produto> listarProdutos() throws SQLException {	
		return produtoService.buscarProdutos();

	}

	@GetMapping("/{id}")
	
	public Produto listarProdutoPorId(@PathVariable int id) throws SQLException {
		List<Produto> produtos = produtoService.buscarProdutos();

		for (Produto produto : produtos) {
			if (produto.getId() == id) {
				return produto;
			}
		}

		return null;
	}

	@PostMapping
	public void inserirProduto(@RequestBody Produto produto) throws SQLException {
		produtoService.inserirProduto(produto);
	}

	
	
}
